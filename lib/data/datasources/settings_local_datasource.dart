import 'package:shared_preferences/shared_preferences.dart';

import '../../presentation/setting/models/tax_model.dart';

class SettingsLocalDatasource {
  //save tax
  Future<bool> saveTax(TaxModel taxModel) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.setString('tax', taxModel.toJson());
  }

  //get
  Future<TaxModel?> getTax() async {
    final prefs = await SharedPreferences.getInstance();
    final tax = prefs.getString('tax');
    if (tax != null) {
      return TaxModel.fromJson(tax);
    } else {
      return TaxModel(name: 'Tax', type: TaxType.pajak, value: 11);
    }
  }

  //save service charge
  Future<bool> saveServiceCharge(int serviceCharge) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.setInt('serviceCharge', serviceCharge);
  }

  //get service charge
  Future<int> getServiceCharge() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getInt('serviceCharge') ?? 0;
  }
}
