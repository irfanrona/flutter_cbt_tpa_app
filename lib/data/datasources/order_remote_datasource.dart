import 'package:http/http.dart' as http;

import '../../core/constants/variables.dart';
import '../../presentation/home/models/order_model.dart';
import 'auth_local_datasource.dart';

class OrderRemoteDatasource {
  //save order to remote server
  Future<bool> saveOrder(OrderModel orderModel) async {
    final authData = await AuthLocalDataSource().getAuthData();
    final url = Uri.parse('${Variables.baseUrl}/api/save-order');

    final response = await http.post(url, body: orderModel.toJson(), headers: {
      'authorization': 'Bearer ${authData!.token}',
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    });

    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }
}
