import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_cbt_tpa_app/core/extensions/date_time_ext.dart';
import '../../../core/components/custom_date_picker.dart';
import '../../../core/components/dashed_line.dart';
import '../../../core/components/spaces.dart';
import '../bloc/transaction_report/transaction_report_bloc.dart';
import '../widgets/report_menu.dart';
import '../widgets/report_title.dart';

class TranscationReportPage extends StatefulWidget {
  const TranscationReportPage({super.key});

  @override
  State<TranscationReportPage> createState() => _TranscationReportPageState();
}

class _TranscationReportPageState extends State<TranscationReportPage> {
  String title = 'Transaction Report';
  DateTime fromDate = DateTime.now().subtract(const Duration(days: 30));
  DateTime toDate = DateTime.now();

  @override
  Widget build(BuildContext context) {
    String searchDateFormatted =
        '${fromDate.toFormattedDate2()} to ${toDate.toFormattedDate2()}';
    return SingleChildScrollView(
      // RIGHT CONTENT
      child: Expanded(
        flex: 2,
        child: Align(
          alignment: Alignment.topCenter,
          child: SingleChildScrollView(
            padding: const EdgeInsets.all(24.0),
            child: BlocBuilder<TransactionReportBloc, TransactionReportState>(
              builder: (context, state) {
                final totalRevenue = state.maybeMap(
                  orElse: () => 0,
                  loaded: (value) {
                    return value.transactionReport.fold(
                      0,
                      (previousValue, element) => previousValue + element.total,
                    );
                  },
                );

                final subTotal = state.maybeMap(
                  orElse: () => 0,
                  loaded: (value) {
                    return value.transactionReport.fold(
                      0,
                      (previousValue, element) =>
                          previousValue + element.subTotal,
                    );
                  },
                );

                final discount = state.maybeMap(
                  orElse: () => 0,
                  loaded: (value) {
                    return value.transactionReport.fold(
                      0,
                      (previousValue, element) =>
                          previousValue + element.discount,
                    );
                  },
                );

                final tax = state.maybeMap(
                  orElse: () => 0,
                  loaded: (value) {
                    return value.transactionReport.fold(
                      0,
                      (previousValue, element) => previousValue + element.tax,
                    );
                  },
                );
                return state.maybeWhen(orElse: () {
                  return const Center(
                    child: Text('No Data'),
                  );
                }, loading: () {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }, loaded: (transactionReport) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Center(
                        child: Text(
                          title,
                          style: const TextStyle(
                              fontWeight: FontWeight.w600, fontSize: 16.0),
                        ),
                      ),
                      Center(
                        child: Text(
                          searchDateFormatted,
                          style: const TextStyle(fontSize: 16.0),
                        ),
                      ),
                      const SpaceHeight(16.0),

                      // PAYMENT INFO
                      ...[
                        const Text('PAYMENT'),
                        const SpaceHeight(8.0),
                        const DashedLine(),
                        const DashedLine(),
                        const SpaceHeight(8.0),
                        const Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('Cash'),
                            Text('0'),
                          ],
                        ),
                        const SpaceHeight(8.0),
                        const DashedLine(),
                        const DashedLine(),
                        const SpaceHeight(8.0),
                        const Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('TOTAL'),
                            Text('0'),
                          ],
                        ),
                      ],
                    ],
                  );
                });
              },
            ),
          ),
        ),
      ),
    );
  }
}
