import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_cbt_tpa_app/core/extensions/date_time_ext.dart';
import 'package:flutter_cbt_tpa_app/presentation/report/pages/daily_report_page.dart';
import 'package:flutter_cbt_tpa_app/presentation/report/pages/item_report_page.dart';
import 'package:flutter_cbt_tpa_app/presentation/report/pages/summary_report_page.dart';
import 'package:flutter_cbt_tpa_app/presentation/report/pages/transaction_report_page.dart';
import '../../../core/components/custom_date_picker.dart';
import '../../../core/components/dashed_line.dart';
import '../../../core/components/spaces.dart';
import '../../setting/pages/discount_page.dart';
import '../../setting/pages/manage_printer_page.dart';
import '../../setting/pages/sync_data_page.dart';
import '../../setting/pages/tax_page.dart';
import '../bloc/transaction_report/transaction_report_bloc.dart';
import '../widgets/report_menu.dart';
import '../widgets/report_title.dart';

class ReportPage extends StatefulWidget {
  const ReportPage({super.key});

  @override
  State<ReportPage> createState() => _ReportPageState();
}

class _ReportPageState extends State<ReportPage> {
  int selectedMenu = 0;
  String title = 'Summary Sales Report';
  DateTime fromDate = DateTime.now().subtract(const Duration(days: 30));
  DateTime toDate = DateTime.now();

  @override
  Widget build(BuildContext context) {
    String searchDateFormatted =
        '${fromDate.toFormattedDate2()} to ${toDate.toFormattedDate2()}';
    return Scaffold(
      body: Row(
        children: [
          // LEFT CONTENT
          Expanded(
            flex: 3,
            child: Align(
              alignment: Alignment.topLeft,
              child: SingleChildScrollView(
                padding: const EdgeInsets.all(24.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const ReportTitle(),
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Flexible(
                            child: CustomDatePicker(
                              prefix: const Text('From: '),
                              initialDate: fromDate,
                              onDateSelected: (selectedDate) {
                                fromDate = selectedDate;
                                setState(() {});
                              },
                            ),
                          ),
                          const SpaceWidth(100.0),
                          Flexible(
                            child: CustomDatePicker(
                              prefix: const Text('To: '),
                              initialDate: toDate,
                              onDateSelected: (selectedDate) {
                                toDate = selectedDate;
                                setState(() {});
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Wrap(
                        children: [
                          ReportMenu(
                            label: 'Transaction Report',
                            onPressed: () {
                              selectedMenu = 0;
                              title = 'Transaction Report';
                              setState(() {});
                              //enddate is 1 month before the current date
                              context.read<TransactionReportBloc>().add(
                                  TransactionReportEvent.getReport(
                                      startDate: DateTime.now(),
                                      endDate: DateTime.now()
                                          .subtract(const Duration(days: 30))));
                            },
                            isActive: selectedMenu == 0,
                          ),
                          ReportMenu(
                            label: 'Item Sales Report',
                            onPressed: () {
                              selectedMenu = 1;
                              title = 'Item Sales Report';
                              setState(() {});
                            },
                            isActive: selectedMenu == 1,
                          ),
                          ReportMenu(
                            label: 'Daily Sales Report',
                            onPressed: () {
                              selectedMenu = 2;
                              title = 'Daily Sales Report';
                              setState(() {});
                            },
                            isActive: selectedMenu == 2,
                          ),
                          ReportMenu(
                            label: 'Summary Sales Report',
                            onPressed: () {
                              selectedMenu = 3;
                              title = 'Summary Sales Report';
                              setState(() {});
                            },
                            isActive: selectedMenu == 3,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),

          // RIGHT CONTENT
          Expanded(
            flex: 2,
            child: Align(
              alignment: Alignment.topCenter,
              child: SingleChildScrollView(
                padding: const EdgeInsets.all(24.0),
                child: IndexedStack(
                  index: selectedMenu,
                  children: const [
                    TranscationReportPage(),
                    ItemReportPage(),
                    DailyReportPage(),
                    SummaryReportPage(),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
