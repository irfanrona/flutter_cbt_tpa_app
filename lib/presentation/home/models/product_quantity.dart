// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import '../../../data/models/response/product_response_model.dart';

class ProductQuantity {
  int quantity;
  final Product product;

  ProductQuantity({required this.quantity, required this.product});

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ProductQuantity &&
        other.quantity == quantity &&
        other.product == product;
  }

  @override
  int get hashCode => product.hashCode ^ quantity.hashCode;

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'quantity': quantity,
      'product': product.toMap(),
    };
  }

  Map<String, dynamic> toLocalMap(int orderId) {
    return <String, dynamic>{
      'id_order': orderId,
      'id_product': product.id,
      'quantity': quantity,
      'price': product.price,
    };
  }

  factory ProductQuantity.fromMap(Map<String, dynamic> map) {
    return ProductQuantity(
      quantity: map['quantity']?.toInt() ?? 0,
      product: Product.fromMap(map['product']),
    );
  }

  factory ProductQuantity.fromLocalMap(Map<String, dynamic> map) {
    return ProductQuantity(
      quantity: map['quantity']?.toInt() ?? 0,
      product: Product.fromOrderMap(map),
    );
  }

  String toJson() => json.encode(toMap());

  factory ProductQuantity.fromJson(String source) =>
      ProductQuantity.fromMap(json.decode(source));
}
