import 'package:bloc/bloc.dart';
import 'package:flutter_cbt_tpa_app/core/extensions/string_ext.dart';
import 'package:flutter_cbt_tpa_app/data/datasources/auth_local_datasource.dart';
import 'package:flutter_cbt_tpa_app/data/datasources/product_local_datasource.dart';
import 'package:flutter_cbt_tpa_app/presentation/home/models/product_quantity.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:intl/intl.dart';

import '../../models/order_model.dart';

part 'order_event.dart';
part 'order_state.dart';
part 'order_bloc.freezed.dart';

class OrderBloc extends Bloc<OrderEvent, OrderState> {
  OrderBloc() : super(const _Initial()) {
    on<_Order>((event, emit) async {
      emit(const _Loading());
      //save to local storage
      final subTotal = event.items.fold<int>(
          0,
          (previousValue, element) =>
              previousValue +
              (element.product.price!.toIntegerFromText * element.quantity));
      final totalItem = event.items.fold<int>(
          0, (previousValue, element) => previousValue + element.quantity);
      final userData = await AuthLocalDataSource().getAuthData();
      final dataInput = OrderModel(
        subTotal: subTotal,
        paymentAmount: event.paymentAmount,
        tax: event.tax,
        discount: event.discount,
        serviceCharge: event.serviceCharge,
        total: (subTotal - event.discount) + event.tax + event.serviceCharge,
        paymentMethod: 'Cash',
        totalItem: totalItem,
        idKasir: userData!.user!.id!,
        namaKasir: userData.user!.name!,
        transactionTime: DateFormat('YYYY-MM-dd').format(DateTime.now()),
        isSync: 0,
        orderItems: event.items,
      );

      await ProductLocalDatasource.instance.saveOrder(dataInput);

      emit(_Loaded(dataInput));
    });
  }
}
