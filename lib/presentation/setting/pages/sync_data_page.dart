import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_cbt_tpa_app/data/datasources/product_local_datasource.dart';
import 'package:flutter_cbt_tpa_app/data/models/response/product_response_model.dart';
import 'package:flutter_cbt_tpa_app/presentation/setting/bloc/sync_product/sync_product_bloc.dart';

import '../bloc/sync_order/sync_order_bloc.dart';

class SyncDataPage extends StatefulWidget {
  const SyncDataPage({super.key});

  @override
  State<SyncDataPage> createState() => _SyncDataPageState();
}

class _SyncDataPageState extends State<SyncDataPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Sync Data'),
      ),
      body: Column(
        children: [
          BlocConsumer<SyncProductBloc, SyncProductState>(
            listener: (context, state) {
              state.maybeWhen(
                  orElse: () {},
                  error: (message) {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Text(message),
                      backgroundColor: Colors.red,
                    ));
                  },
                  loaded: (productResponseModel) {
                    ProductLocalDatasource.instance.deleteAllProduct();
                    ProductLocalDatasource.instance.insertProducts(
                      productResponseModel.data!,
                    );
                    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                      content: Text('Data Synced'),
                      backgroundColor: Colors.green,
                    ));
                  });
            },
            builder: (context, state) {
              return state.maybeWhen(orElse: () {
                return ElevatedButton(
                    onPressed: () {
                      context
                          .read<SyncProductBloc>()
                          .add(const SyncProductEvent.syncProduct());
                    },
                    child: const Text('Sync Data'));
              }, loading: () {
                return const Center(child: CircularProgressIndicator());
              });
            },
          ),
          BlocConsumer<SyncOrderBloc, SyncOrderState>(
            listener: (context, state) {
              state.maybeWhen(
                  orElse: () {},
                  error: (message) {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Text(message),
                      backgroundColor: Colors.red,
                    ));
                  },
                  loaded: () {
                    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                      content: Text('Synced Order Success'),
                      backgroundColor: Colors.green,
                    ));
                  });
            },
            builder: (context, state) {
              return state.maybeWhen(
                orElse: () {
                  return ElevatedButton(
                      onPressed: () {
                        context.read<SyncOrderBloc>().add(
                              const SyncOrderEvent.syncOrder(),
                            );
                      },
                      child: const Text('Sync Order'));
                },
                loading: () {
                  return const Center(child: CircularProgressIndicator());
                },
              );
            },
          )
        ],
      ),
    );
  }
}
